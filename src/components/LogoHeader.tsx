import React from 'react';
import Image from 'next/image';

import { DigiLogo } from '@digi/arbetsformedlingen-react';
import { LogoColor, LogoVariation } from '@digi/arbetsformedlingen';

type Props = {
  partnerLogoUrl: string | undefined;
};

const LogoHeader = ({ partnerLogoUrl }: Props) => {
  return (
    <div className="flex flex-col items-center md:flex-row md:justify-center" data-testid="logoHeader">
      <div className="m-8">
        <DigiLogo afVariation={LogoVariation.LARGE} afColor={LogoColor.PRIMARY} data-testid="afLogo" />
      </div>
      {partnerLogoUrl && (
        <Image src={partnerLogoUrl} width={64} height={64} alt="Partner logotype" data-testid="partnerLogoURL" />
      )}
    </div>
  );
};

export default LogoHeader;
