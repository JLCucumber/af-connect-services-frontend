import { describe, expect, it, vi } from "vitest";
import { getLogLevel, getLogger, send } from "./logger";
import { type LogEvent } from "pino";

const { mockPino } = vi.hoisted(() => ({
  mockPino: vi.fn()
}))

vi.mock('pino', () => ({ default: mockPino }))

vi.mock('../../logging.config', () => ({
  default: {
    '*': 'example-default-value',
    'module': 'example-module-value'
  }
}))


describe('getLogLevels', () => {
  it('should use logging.config.ts for configuration', () => {
    expect(getLogLevel('*')).toBe('example-default-value');
    expect(getLogLevel('module')).toBe('example-module-value');
  })

  it('should return log level when set', () => {
    const logLevelsMap = new Map([['*', 'error']]);
    const logLevel = getLogLevel('*', logLevelsMap);

    expect(logLevel).toBe('error')
  })

  it('should return default log level when not set', () => {
    const logLevelsMap = new Map<string, string>([]);
    const logLevel = getLogLevel('*', logLevelsMap)

    expect(logLevel).toBe('info')
  })

  it('should return log level for named module when set', () => {
    const logLevelsMap = new Map([['*', 'info'], ['module', 'warn']])
    const logLevel = getLogLevel('module', logLevelsMap)

    expect(logLevel).toBe('warn')
  })
})

describe('getLogger', () => {
  it('should create logger', () => {
    mockPino.mockReturnValueOnce('example-pino-logger')

    const logger = getLogger('module');

    expect(mockPino).toBeCalledWith({ level: 'example-module-value', name: 'module', enabled: false })
    expect(logger).toBe('example-pino-logger');
  })

  it('should create logger for frontend', () => {
    mockPino.mockReturnValueOnce('example-pino-logger')
    const actualWindow = global.window
    global.window = {} as typeof global.window

    const logger = getLogger('module');

    expect(mockPino).toBeCalledWith({ level: 'example-module-value', name: 'module', enabled: true, browser: { transmit: { send: send } } })
    expect(logger).toBe('example-pino-logger');

    // Restore window
    global.window = actualWindow
  })
})

describe('send', () => {
  it('should send log messages', () => {
    const actualNavigator = global.navigator
    global.navigator = {} as typeof global.navigator

    const mockSendBeacon = vi.fn();
    global.navigator.sendBeacon = mockSendBeacon

    const mockBlob = vi.fn().mockImplementation((...args: unknown[]) => { return { args } })
    const actualBlob = global.Blob
    global.Blob = mockBlob

    const logEvent = {
      ts: 1703057462,
      messages: ['Some log message.'],
      level: {
        label: 'info',
        value: 30
      }
    } as LogEvent

    send('error', logEvent)

    expect(mockSendBeacon).toBeCalledWith('/api/log', { args: [["{\"msg\":[\"Some log message.\"],\"level\":\"error\"}"], { type: 'application/json' }] })

    global.navigator = actualNavigator
    global.Blob = actualBlob
  })

})
