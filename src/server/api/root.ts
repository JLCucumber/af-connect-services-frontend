import { consentRouter } from './routers/consent';
import { serviceRouter } from './routers/service';
import { createTRPCRouter } from './trpc';

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  consent: consentRouter,
  service: serviceRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
