/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { afterAll, beforeAll, afterEach, describe, expect, it, vi } from 'vitest';
import { appRouter } from '../root';
import { createInnerTRPCContext } from '../trpc';

describe('service router', () => {
  const mockFetch = vi.fn();

  beforeAll(() => {
    vi.stubGlobal('fetch', mockFetch);
  });

  afterEach(() => {
    mockFetch.mockReset();
  });

  afterAll(() => {
    vi.unstubAllGlobals();
  });

  describe('getInfo', () => {

    it('getInfo should call backend and return with service details', async () => {
      mockFetch.mockResolvedValue({
        ok: true,
        json: vi.fn(() => ({
          clientId: "ABCxyz-000001",
          name: "ABC",
          redirectUrl: "http://example.se/redirect",
          created: 1695123120365,
          updated: 1695123120365
        })),
      });

      const ctx = createInnerTRPCContext({ user: undefined });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'ABCxyz-000001',
      };

      const request = await caller.service.getInfo(input);

      expect(mockFetch).toBeCalledTimes(1);
      expect(mockFetch.mock.calls[0][0]).toBe(
        'http://localhost:4000/service/ABCxyz-000001/',
      );

      expect(request?.id).toBe('ABCxyz-000001');
      expect(request?.name).toBe('ABC');
      expect(request?.logoUrl).toBe('./ABCxyz-000001.svg');
    });

    it('getInfo should return error message on failure', async () => {
      mockFetch.mockResolvedValue({
        ok: false,
        status: 500,
        statusText: 'Internal server error.'
      });

      const ctx = createInnerTRPCContext({ user: undefined });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'abc-000001',
      };

      const request = await caller.service.getInfo(input);

      expect(mockFetch).toBeCalledTimes(1);
      expect(mockFetch.mock.calls[0][0]).toBe(
        'http://localhost:4000/service/abc-000001/',
      );

      expect(request.message).toBe('Failed to fetch client details.');
    });
  });
});