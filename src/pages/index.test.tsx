// @vitest-environment jsdom
import React from 'react';
import { render, waitFor } from '@testing-library/react';
import Index from './index.page';
import { vi, describe, expect, it } from 'vitest';

const { mockUseRouter, mockServiceDetails } = vi.hoisted(() => ({
  mockUseRouter: vi.fn(), //vi.fn()hoistas tillsammans med vi.mock
  mockServiceDetails: vi.fn(), //vi.fn()hoistas tillsammans med vi.mock
})); //vi.mock('next/router) hoistas

vi.mock('next/router', () => ({
  useRouter: mockUseRouter,
}));
//   () => ({
//     query: {
//         clientId: 'testId',
//         scope: 'testScope',
//         state: 'testState'}, //tittar efter query parametrar, servicedetails gör inte det
//   }),
// ));

vi.mock('@app/utils/logger', () => ({
  getLogger: () => ({
    info: vi.fn(),
  }),
}));

vi.mock('@app/components/ServiceDetails', () => ({
  default: mockServiceDetails,
}));

describe('Index Page', () => {
  it('renders error for missing clientId', () => {
    mockUseRouter.mockImplementationOnce(() => ({
      query: {
        // clientId: 'testId'
        scope: 'testScope',
        state: 'testState',
      },
    }));
    const { queryByTestId } = render(<Index />);
    expect(queryByTestId('paramErrorMessage')).toHaveTextContent('Error, query parameter clientId missing');
  });

  it('renders error for missing scope', () => {
    mockUseRouter.mockImplementationOnce(() => ({
      query: {
        clientId: 'testId',
        // scope: 'testScope',
        state: 'testState',
      },
    }));
    const { queryByTestId } = render(<Index />);
    expect(queryByTestId('paramErrorMessage')).toHaveTextContent('Error, query parameter scope missing');
  });

  it('renders error for missing state', () => {
    mockUseRouter.mockImplementationOnce(() => ({
      query: {
        clientId: 'testId',
        scope: 'testScope',
        // state: 'testState',
      },
    }));
    const { queryByTestId } = render(<Index />);
    expect(queryByTestId('paramErrorMessage')).toHaveTextContent('Error, query parameter state missing');
  });

  it('renders error for missing multiple parameters', () => {
    mockUseRouter.mockImplementationOnce(() => ({
      query: {
        clientId: 'testId',
        // scope: 'testScope',
        // state: 'testState',
      },
    }));
    const { queryByTestId } = render(<Index />);
    expect(queryByTestId('paramErrorMessage')).toHaveTextContent('Error, query parameter scope, state missing');
  });

  it('renders error for invalid scope', () => {
    mockUseRouter.mockImplementationOnce(() => ({
      query: {
        clientId: 'testId',
        scope: 'invalidTestScope',
        state: 'testState',
      },
    }));
    const { queryByTestId } = render(<Index />);
    expect(queryByTestId('scopeErrorMessage')).toHaveTextContent('Error, scope not one of query-unemployment');
  }); //detta test letar efter en div-tagg

  it('renders ServiceDetails valid input', async () => {
    mockUseRouter.mockImplementationOnce(() => ({
      query: {
        clientId: 'testId',
        scope: 'query-unemployment',
        state: 'testState',
      },
    }));
    render(<Index />);
    await waitFor (()=>{
    expect(mockServiceDetails).toHaveBeenCalledWith({
      clientId: 'testId',
      foreignUserId: 'testState',
      scope: 'query-unemployment',
    }, {})});
  });
});
