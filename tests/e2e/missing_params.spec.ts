import { test, expect, type Page } from '@playwright/test';

let page: Page;

test.beforeAll(async ({ browser }) => {
  page = await browser.newPage();

  await page.goto('/');

  await page.getByRole('combobox').click();
  await page.getByLabel('9010102383').check();

  await page.getByRole('button', { name: 'Logga in' }).click();
})

test.afterAll(async () => {
  await page.close();
})

test('should render error message on index page when missing parameters', async () => {
  await page.goto('/');

  await expect(page.locator('body')).toContainText('Error, query parameter clientId, scope, state missing.');
});

test('should render error message on index page when missing clientId parameter', async () => {
  await page.goto('/?state=efgh');

  await expect(page.locator('body')).toContainText('Error, query parameter clientId, scope missing.');
});

test('should render error message on index page when missing state parameter', async () => {
  await page.goto('/?clientId=abcd');

  await expect(page.locator('body')).toContainText('Error, query parameter scope, state missing.');
});

test('should render error message on index page when missing scope parameter', async () => {
  await page.goto('/?scope=query-unemployment');

  await expect(page.locator('body')).toContainText('Error, query parameter clientId, state missing.');
});

test('should render index page', async () => {
  await page.goto('/?clientId=abcd&state=efgh&scope=query-unemployment');

  await expect(page.locator('body')).toContainText('Din data - ditt val');
});
