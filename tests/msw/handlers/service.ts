import { trpcMsw } from "../mockTrpc"

export const getInfo = () => {
  return trpcMsw.service.getInfo.query(async (_req, res, ctx) => {
    return res(ctx.status(200), ctx.data({ id: 'abc-000001', name: 'Försäkringsbolag AB', logoUrl: './abc-000001.svg' }))
  })
}