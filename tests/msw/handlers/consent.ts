import { trpcMsw } from "../mockTrpc"

export const giveConsentHandler = () => {
  return trpcMsw.consent.giveConsent.mutation(async (req, res, ctx) => {
    const input = await req.json();
    const authCode = '1c0616eb-4297-4e92-ac26-4906f16fa7e7'
    return res(ctx.status(200), ctx.data({ message: 'OK', redirectUrl: `http://example.com/return?authCode=${authCode}&state=${input.foreignUserId}&message=OK` }))
  })
}

export const giveConsentHandlerError = () => {
  return trpcMsw.consent.giveConsent.mutation(async (req, res, ctx) => {
    return res(ctx.status(200), ctx.data({ message: 'Failed to provide consent, invalid response from back end.' }))
  })
}

export const revokeConsentHandler = () => {
  return trpcMsw.consent.revokeConsent.mutation(async (req, res, ctx) => {
    const input = await req.json();

    return res(ctx.status(200), ctx.data({ message: 'OK', redirectUrl: `http://example.com/return?authCode=&state=${input.foreignUserId}&message=Error` }))
  })
}

export const revokeConsentHandlerError = () => {
  return trpcMsw.consent.revokeConsent.mutation(async (req, res, ctx) => {
    return res(ctx.status(200), ctx.data({ message: 'Failed to revoke consent, invalid response from back end.' }))
  })
}
