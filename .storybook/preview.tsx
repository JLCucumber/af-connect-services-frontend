import React, { useState } from 'react';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { createTRPCReact, httpBatchLink } from '@trpc/react-query';
import { initialize, mswDecorator } from 'msw-storybook-addon';
import { AppRouter } from '../src/server/api/root';
import superjson from 'superjson';

import type { Decorator, Preview } from '@storybook/react';

import { withThemeByClassName } from '@storybook/addon-themes';

import '../src/styles/globals.css';

initialize({
  onUnhandledRequest: 'bypass',
});

const trpc = createTRPCReact<AppRouter>();

const SetupDecorator: Decorator = (Story) => {
  const [queryClient] = useState(
    new QueryClient({
      defaultOptions: {
        queries: {
          staleTime: Infinity,
          retry: false,
          refetchOnWindowFocus: false,
        },
      },
    }),
  );
  const [trpcClient] = useState(() =>
    trpc.createClient({
      links: [httpBatchLink({ url: '' })],
      transformer: superjson,
    }),
  );
  return (
    <trpc.Provider client={trpcClient} queryClient={queryClient}>
      <QueryClientProvider client={queryClient}>
        <Story />
      </QueryClientProvider>
    </trpc.Provider>
  );
};

const customViewports = {
  mobile: {
    name: 'Mobiltelefon',
    styles: {
      height: '667px',
      width: '375px',
    },
    type: 'mobile',
  },
  tablet: {
    name: 'Surfplatta',
    styles: {
      height: '1024px',
      width: '768px',
    },
    type: 'tablet',
  },
  desktop: {
    name: 'Dator',
    styles: {
      height: '675px',
      width: '1200px',
    },
    type: 'desktop',
  },
};

const preview: Preview = {
  parameters: {
    options: {
      storySort: {
        method: 'alphabetical',
        order: ['Overview', 'Components'],
      },
    },
    viewport: {
      viewports: customViewports,
    },
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  decorators: [
    mswDecorator,
    SetupDecorator,
    withThemeByClassName({
      themes: {
        light: 'light',
        dark: 'dark',
      },
      defaultTheme: 'light',
    }),
  ],
};

export default preview;
